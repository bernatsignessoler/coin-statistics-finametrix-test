# coin-statistics-finametrix-test

Coin Statistics allows to know all the updated information of the main cryptocurrencies in the market.<br>
This demo-test is made with React.js starting from npm init react-app (https://github.com/facebook/create-react-app). <br>

## Before start

You will need to have installed [npm](https://www.npmjs.com/get-npm). Then proceed to clone the project with
`git clone https://gitlab.com/bernatsignessoler/coin-statistics-finametrix-test.git`.<br>

Once the project have been cloned, you'll need to install all the dependencies with `npm install` in the root folder.<br>

Additionally you must create a `.env` file in the root folder and add your (https://www.alphavantage.co/support/#api-key) api key with this format: `REACT_APP_ALPHAVANTAGE_API_KEY=<your-apy-key>`.<br>

**WARNING: Environment variables are embedded into the build, meaning anyone can view them by inspecting your app's files.**

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>

### `npm run build`

Builds the app for production to the `build` folder.<br>

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## App Guide

### Translations

You can change the page language choosing your option in the right top corner

**Note: This app stores the selected language in the browser for future access!**

### Filters

You can filter by name, sort by market cap, close and volume, and finally change the exchange market.
If some coin isnt load, will start a count down to reload the coin when the limitation ends.

### Details

When one cryptocurrency is clicked the details modal appears showing a chart which can be customised. Additionally you can change the current selected cryptocurrency.

### 404

All routes out of `/` will redirect to 404 page.
