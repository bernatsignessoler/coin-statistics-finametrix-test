import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { IntlProvider, addLocaleData } from 'react-intl';
import locale_en from 'react-intl/locale-data/en';
import locale_es from 'react-intl/locale-data/es';

import laguangeManager from './core/utils/languageUtil';

import { LanguageContextProvider } from './core/contexts/languageContext';

import messages_es from './translations/es.json';
import messages_en from './translations/en.json';

import Home from './components';
import NotFound from './components/NotFound';

import './App.css';

const messages = {
  es: messages_es,
  en: messages_en
};

addLocaleData([...locale_en, ...locale_es]);

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      language: laguangeManager.getLang()
    };

    this.handleSelectLang = this.handleSelectLang.bind(this);
  }

  handleSelectLang(lang) {
    laguangeManager.setLang(lang);
    this.setState({ language: laguangeManager.getLang() });
  }

  componentDidCatch() {
    // TO-IMPLEMENT
  }

  render() {
    const { language } = this.state;

    return (
      <IntlProvider locale={language} messages={messages[language]}>
        <LanguageContextProvider
          value={{ language, onSelectLanguage: this.handleSelectLang }}
        >
          <Router>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route component={NotFound} />
            </Switch>
          </Router>
        </LanguageContextProvider>
      </IntlProvider>
    );
  }
}

export default App;
