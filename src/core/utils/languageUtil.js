import UtilFactory from '../UtilFactory';
import { ESP, ENG } from '../constants/languages';

const getBrowserLang = () => {
  let lang = navigator.language.split('-')[0];
  if (![ENG, ESP].includes(lang)) lang = ENG;

  return lang;
};

const setLang = lang => {
  if (![ENG, ESP].includes(lang)) lang = ENG;
  window.localStorage.setItem('cs-lang-token', lang);
};

const getLang = () => window.localStorage.getItem('cs-lang-token') || getBrowserLang();

export default {
  setLang: (...params) => new UtilFactory(setLang, params).runWithHandle(),
  getLang: () => new UtilFactory(getLang).runWithHandle()
};
