import UtilFactory from '../UtilFactory';

const defaultTranslate = (identifier, intl, options) =>
  intl.formatMessage({ id: identifier }, options);

export default {
  defaultTranslate: (...params) =>
    new UtilFactory(defaultTranslate, params).runWithHandle()
};
