import UtilFactory from '../UtilFactory';

import AvApi from '../api/alphavantageApi';

import {
  ALPHAVANTAGE_DC_DAILY_FUNC,
  ALPHAVANTAGE_API_KEY
} from '../constants/alphavantage';

const generateQuery = params => {
  let query = '?';

  Object.keys(params).forEach(key => {
    const value = params[key];

    query = query.concat(`${key}=${value}&`);
  });

  return query.substring(0, query.length - 1);
};

const formatData = (data, cryptoCurrencyCode) => {
  const {
    'Meta Data': metaData,
    'Time Series (Digital Currency Daily)': dailyCurrency,
    Note
  } = data;

  if (Note) {
    return { currencyCode: cryptoCurrencyCode, Note };
  }

  const {
    '2. Digital Currency Code': currencyCode,
    '3. Digital Currency Name': currencyName,
    '4. Market Code': marketCode,
    '5. Market Name': marketName,
    '6. Last Refreshed': lastRefreshed
  } = metaData;

  let lastCurrencyDate = lastRefreshed.split(' ')[0];

  const {
    [`4a. close (${marketCode})`]: closeMarket,
    '4b. close (USD)': closeUSD,
    '5. volume': volume,
    '6. market cap (USD)': marketCap
  } = dailyCurrency[`${lastCurrencyDate}`];

  const lastCurrency = { closeMarket, closeUSD, volume, marketCap };

  let formattedDailyCurrency = Object.keys(dailyCurrency).map(key => {
    const {
      [`2a. high (${marketCode})`]: highMarket,
      [`4a. close (${marketCode})`]: closeMarket,
      '4b. close (USD)': closeUSD,
      '5. volume': volume,
      '6. market cap (USD)': marketCap
    } = dailyCurrency[key];

    return { date: key, highMarket, closeMarket, closeUSD, volume, marketCap };
  });

  return {
    currencyCode,
    currencyName,
    marketCode,
    marketName,
    lastCurrencyDate,
    lastCurrency,
    dailyCurrency: formattedDailyCurrency.reverse()
  };
};

const getDailyCurrency = (cryptoCurrencyCode, exchangeMarket) => {
  const query = generateQuery({
    function: ALPHAVANTAGE_DC_DAILY_FUNC,
    symbol: cryptoCurrencyCode,
    market: exchangeMarket,
    apikey: ALPHAVANTAGE_API_KEY
  });

  return new Promise((resolve, reject) => {
    AvApi.getData(query)()
      .then(({ data }) => resolve(formatData(data, cryptoCurrencyCode)))
      .catch(err => reject(err));
  });
};

export default {
  getDailyCurrency: (...params) =>
    new UtilFactory(getDailyCurrency, params).runWithHandle()
};
