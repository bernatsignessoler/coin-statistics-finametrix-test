import React, { Component } from 'react';

const LanguageContext = React.createContext({
  language: undefined,
  onSelectLanguage: undefined
});

export const LanguageContextProvider = LanguageContext.Provider;

export const applyLanguageContext = CurrentComponent =>
  class HighOrderComponent extends Component {
    render() {
      return (
        <LanguageContext.Consumer>
          {({ language, onSelectLanguage }) => (
            <CurrentComponent
              {...this.props}
              language={language}
              onSelectLanguage={onSelectLanguage}
            />
          )}
        </LanguageContext.Consumer>
      );
    }
  };
