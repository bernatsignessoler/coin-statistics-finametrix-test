class UtilFactory {
  constructor(fn, params) {
    this.fn = fn;
    this.params = params;
  }

  runWithHandle = () => {
    try {
      if (this.params) {
        return this.fn(...this.params);
      }

      return this.fn();
    } catch (error) {
      return error; // TO-IMPLEMENT
    }
  };
}

export default UtilFactory;
