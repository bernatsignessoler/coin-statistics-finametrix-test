import axios from 'axios';
import { ALPHAVANTAGE_URL } from '../constants/alphavantage';

const apiPath = '/query';

class AvApi {
  constructor() {
    this.hostUrl = ALPHAVANTAGE_URL;
    this.baseUrl = this.hostUrl + apiPath;
  }

  getData(path) {
    return async () => await axios.get(`${this.baseUrl}${path}`);
  }
}

export default new AvApi();
