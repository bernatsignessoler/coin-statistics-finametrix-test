import React from 'react';
import PropTypes from 'prop-types';

import './styles.css';

const SIZE_VALUES = [
  'ex-large-cs-title-size',
  'large-cs-title-size',
  'default-cs-title-size'
];

const CsTitle = ({ size, className, children }) => {
  if (!children) {
    throw new Error('CSTITLE ERROR: COMPONENT NEEDS CHILDREN TO RENDER CORRECTLY');
  }

  if (!SIZE_VALUES.includes(size)) {
    throw new Error('CSTITLE ERROR: SIZE MUST BE STRING. PLEASE CHECK SIZE_VALUES LIST');
  }

  return <span className={`${size} ${className}`}>{children}</span>;
};

export default CsTitle;

CsTitle.propTypes = {
  children: PropTypes.string.isRequired,
  size: PropTypes.string,
  className: PropTypes.string
};

CsTitle.defaultProps = {
  size: 'default-cs-title-size',
  className: ''
};
