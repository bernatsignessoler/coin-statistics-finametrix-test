import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';

import translator from '../../core/utils/translationUtil';
import coinManager from '../../core/utils/cryptoCoinUtil';

import Filters from './components/Filters';
import CryptoCard from './components/CryptoCard';
import CryptoModal from './components/CryptoModal';

import CsTitle from '../../core/uikit/CsTitle';

import './styles.css';

import {
  TOP_TEN_CRYPTOCURRENCIES,
  EXCHANGE_MARKET_OPTIONS
} from '../../core/constants/alphavantage';

const ONE_MIN = 60000;

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cryptos: [],
      toRefresh: [],
      countDown: false,
      exMarket: EXCHANGE_MARKET_OPTIONS.EUR,
      isModalVisible: false,
      currentCryptoSelected: {}
    };
    this.restrictionTimer = {};
    this.setRestrictionTimer = this.setRestrictionTimer.bind(this);
    this.fetchCrypto = this.fetchCrypto.bind(this);
    this.refreshCryptos = this.refreshCryptos.bind(this);
    this.sortCryptos = this.sortCryptos.bind(this);
    this.filterCryptos = this.filterCryptos.bind(this);
    this.handleExchangeMarketChange = this.handleExchangeMarketChange.bind(this);
    this.handleShowModal = this.handleShowModal.bind(this);
    this.handleChangeSelectedCrypto = this.handleChangeSelectedCrypto.bind(this);
  }

  componentDidMount() {
    TOP_TEN_CRYPTOCURRENCIES.map(currencyCode =>
      this.fetchCrypto(currencyCode, this.state.exMarket)
    );
  }

  componentWillUnmount() {
    clearInterval(this.restrictionTimer);
  }

  setRestrictionTimer() {
    clearInterval(this.restrictionTimer);
    this.setState({ countDown: Date.now() + ONE_MIN });
    this.restrictionTimer = setInterval(this.refreshCryptos, ONE_MIN);
  }

  refreshCryptos() {
    const { toRefresh, exMarket } = this.state;

    clearInterval(this.restrictionTimer);
    this.setState({ toRefresh: [], countDown: false });
    toRefresh.map(currencyCode => this.fetchCrypto(currencyCode, exMarket));
  }

  fetchCrypto(currencyCode, exchangeMarket) {
    const { cryptos, toRefresh } = this.state;
    let index = cryptos.findIndex(crypto => crypto.currencyCode === currencyCode);
    const initCrypto = {
      currencyCode,
      marketCode: exchangeMarket,
      loading: true,
      display: true,
      fetchErr: false,
      countDown: false
    };

    if (index !== -1) cryptos[index] = initCrypto;
    else index = cryptos.push(initCrypto) - 1;
    this.setState({ cryptos: cryptos });

    coinManager
      .getDailyCurrency(currencyCode, exchangeMarket)
      .then(cryptoCurrency => {
        if (cryptoCurrency.Note) {
          toRefresh.push(cryptoCurrency.currencyCode);
          this.setState({ toRefresh: toRefresh });
          if (!this.state.countDown) this.setRestrictionTimer();
          cryptos[index] = { ...cryptos[index], countDown: this.state.countDown };
        }
        cryptos[index] = { ...cryptoCurrency, ...cryptos[index], loading: false };
        this.setState({ cryptos: cryptos });
      })
      .catch(() => {
        toRefresh.push(currencyCode);
        if (!this.state.countDown) this.setRestrictionTimer();
        cryptos[index] = {
          ...cryptos[index],
          loading: false,
          countDown: this.state.countDown,
          fetchErr: true
        };
        this.setState({ toRefresh: toRefresh, cryptos: cryptos });
      });
  }

  sortCryptos(propName, asc) {
    const { cryptos } = this.state;

    cryptos.sort(({ lastCurrency: c1 }, { lastCurrency: c2 }) => {
      if (!c1 && !c2) return 0;
      if (!c1) return 1;
      if (!c2) return -1;

      return asc ? c1[propName] - c2[propName] : c2[propName] - c1[propName];
    });
    this.setState({ cryptos: cryptos });
  }

  filterCryptos(subString) {
    const { cryptos } = this.state;

    this.setState({
      cryptos: cryptos.map(crypto => {
        const propVal = crypto.currencyName || crypto.Note;
        if (propVal.toLowerCase().includes(subString.toLowerCase()))
          return { ...crypto, display: true };
        else return { ...crypto, display: false };
      })
    });
  }

  handleExchangeMarketChange(exchangeMarket) {
    this.setState({ exMarket: exchangeMarket });
    TOP_TEN_CRYPTOCURRENCIES.map(currencyCode =>
      this.fetchCrypto(currencyCode, exchangeMarket)
    );
  }

  handleShowModal(currCode) {
    const { isModalVisible } = this.state;

    this.setState({ isModalVisible: !isModalVisible });
    if (isModalVisible) {
      this.setState({ currentCryptoSelected: {} });

      return;
    }
    this.handleChangeSelectedCrypto(currCode);
  }

  handleChangeSelectedCrypto(newCurrCode) {
    const { cryptos } = this.state;
    const currentCryptoSelected = cryptos.find(
      ({ currencyCode }) => currencyCode === newCurrCode
    );

    this.setState({ currentCryptoSelected });
  }

  render() {
    const { intl } = this.props;
    const { cryptos, isModalVisible, currentCryptoSelected } = this.state;

    return (
      <Fragment>
        <CsTitle className="main-title" size="ex-large-cs-title-size">
          {translator.defaultTranslate('cryptoStatistics', intl)}
        </CsTitle>
        <Filters
          onSort={this.sortCryptos}
          onFilter={this.filterCryptos}
          onExMarketChange={this.handleExchangeMarketChange}
        />
        <div className={'main-cards-wrapper'}>
          {cryptos.map(crypto =>
            crypto.display ? (
              <CryptoCard
                key={crypto.currencyCode}
                crypto={crypto}
                onClickCard={this.handleShowModal}
              />
            ) : null
          )}
        </div>
        <CryptoModal
          crypto={currentCryptoSelected}
          modalVisible={isModalVisible}
          onCloseModal={this.handleShowModal}
          onCryptoCurrencyChange={this.handleChangeSelectedCrypto}
        />
      </Fragment>
    );
  }
}

export default injectIntl(Main);

Main.propTypes = {
  intl: PropTypes.object.isRequired
};
