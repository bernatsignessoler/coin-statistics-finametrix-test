import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';

import translator from '../../../../core/utils/translationUtil';

import { EXCHANGE_MARKET_OPTIONS } from '../../../../core/constants/alphavantage';

import { Button, Icon, Input, Select } from 'antd';

import './styles.css';

const { Search } = Input;
const { Option } = Select;

const MARKETCAP_VARNAME = 'marketCap';
const CLOSEMARKET_VARNAME = 'closeMarket';
const VOLUME_VARNAME = 'volume';

class Filters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      marketCapDir: true,
      volumeDir: true,
      closeMarketDir: true
    };
  }

  render() {
    const { intl, onSort, onFilter, onExMarketChange } = this.props;
    const { marketCapDir, volumeDir, closeMarketDir } = this.state;

    return (
      <div className="filters-wrapper">
        <Search
          className="filters-search-box"
          placeholder={translator.defaultTranslate('filterNamePlaceholder', intl)}
          onChange={({ target }) => onFilter(target.value)}
        />
        <Button
          className="filters-button-box"
          onClick={() => {
            this.setState(({ marketCapDir }) => ({ marketCapDir: !marketCapDir }));
            onSort(MARKETCAP_VARNAME, marketCapDir);
          }}
        >
          {translator.defaultTranslate('sorterMarketCap', intl)}
          <Icon type={marketCapDir ? 'up' : 'down'} />
        </Button>
        <Button
          className="filters-button-box"
          onClick={() => {
            this.setState(({ closeMarketDir }) => ({ closeMarketDir: !closeMarketDir }));
            onSort(CLOSEMARKET_VARNAME, closeMarketDir);
          }}
        >
          {translator.defaultTranslate('sorterMarketClose', intl)}
          <Icon type={closeMarketDir ? 'up' : 'down'} />
        </Button>
        <Button
          className="filters-button-box"
          onClick={() => {
            this.setState(({ volumeDir }) => ({ volumeDir: !volumeDir }));
            onSort(VOLUME_VARNAME, volumeDir);
          }}
        >
          {translator.defaultTranslate('sorterVolume', intl)}
          <Icon type={volumeDir ? 'up' : 'down'} />
        </Button>
        <Select
          className="filters-select-box"
          onChange={onExMarketChange}
          placeholder={translator.defaultTranslate('selectExMarketPlaceholder', intl)}
        >
          {Object.keys(EXCHANGE_MARKET_OPTIONS).map(key => (
            <Option
              key={EXCHANGE_MARKET_OPTIONS[key]}
              exvalue={EXCHANGE_MARKET_OPTIONS[key]}
            >
              {key}
            </Option>
          ))}
        </Select>
      </div>
    );
  }
}

export default injectIntl(Filters);

Filters.propTypes = {
  intl: PropTypes.object.isRequired,
  onSort: PropTypes.func.isRequired,
  onFilter: PropTypes.func.isRequired,
  onExMarketChange: PropTypes.func.isRequired
};
