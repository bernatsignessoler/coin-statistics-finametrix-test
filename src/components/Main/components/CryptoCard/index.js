import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';

import translator from '../../../../core/utils/translationUtil';

import { Card, Spin, Alert, Statistic } from 'antd';

import './styles.css';

const Countdown = Statistic.Countdown;

const CurrencyCard = ({ intl, crypto, onClickCard }) => {
  const {
    currencyCode,
    marketCode,
    loading,
    fetchErr,
    countDown,
    Note,
    lastCurrency
  } = crypto;

  return (
    <div className="card-wrapper">
      <Card
        className="card-box"
        title={
          <div className="card-title-wrapper">
            <Statistic value={currencyCode} suffix={`/ ${marketCode}`} />
            {countDown && <Countdown value={countDown} suffix="SEC" format="ss" />}
          </div>
        }
        hoverable={!fetchErr}
        onClick={!fetchErr ? () => onClickCard(currencyCode) : null}
      >
        <div
          className={
            loading ? 'card-content-wrapper-cent' : 'card-content-wrapper-nowrap'
          }
        >
          {loading ? (
            <Spin />
          ) : fetchErr ? (
            <Alert
              message={translator.defaultTranslate('networkError', intl)}
              description={translator.defaultTranslate('fetchErrorDesc', intl)}
              type="error"
              showIcon
            />
          ) : Note ? (
            <Alert description={Note} type="warning" showIcon />
          ) : (
            <Fragment>
              <div className="card-body-wrapper">
                <Statistic
                  title={translator.defaultTranslate('sorterMarketClose', intl)}
                  value={lastCurrency.closeMarket}
                  precision={4}
                  suffix={marketCode}
                />
                <Statistic value={lastCurrency.closeUSD} precision={4} suffix="USD" />
              </div>
              <div className="card-body-wrapper">
                <Statistic
                  title={translator.defaultTranslate('sorterMarketCap', intl)}
                  value={lastCurrency.marketCap}
                  precision={4}
                  suffix="USD"
                />
                <Statistic
                  title={translator.defaultTranslate('sorterVolume', intl)}
                  value={lastCurrency.volume}
                  precision={4}
                  suffix="24h"
                />
              </div>
            </Fragment>
          )}
        </div>
      </Card>
    </div>
  );
};

export default injectIntl(CurrencyCard);

CurrencyCard.propTypes = {
  intl: PropTypes.object.isRequired,
  crypto: PropTypes.object.isRequired,
  onClickCard: PropTypes.func.isRequired
};
