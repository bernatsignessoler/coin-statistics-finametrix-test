import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';

import translator from '../../../../../core/utils/translationUtil';

import {
  AreaChart,
  Area,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer
} from 'recharts';

import { Checkbox } from 'antd';

const MARKETCAP_VARNAME = 'marketCap';
const CLOSEMARKET_VARNAME = 'closeMarket';
const HIGHMARKET_VARNAME = 'highMarket';
const VOLUME_VARNAME = 'volume';

class CryptoChart extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selectedOptions: { [CLOSEMARKET_VARNAME]: CLOSEMARKET_VARNAME }
    };
    this.checkOptions = [
      {
        label: translator.defaultTranslate('sorterMarketCap', props.intl),
        value: MARKETCAP_VARNAME
      },
      {
        label: translator.defaultTranslate('sorterMarketClose', props.intl),
        value: CLOSEMARKET_VARNAME
      },
      {
        label: translator.defaultTranslate('highMarketClose', props.intl),
        value: HIGHMARKET_VARNAME
      },
      {
        label: translator.defaultTranslate('sorterVolume', props.intl),
        value: VOLUME_VARNAME
      }
    ];
    this.onCheckGroupChange = this.onCheckGroupChange.bind(this);
  }

  onCheckGroupChange(checkedValues) {
    let selectedOptions = {};

    checkedValues.forEach(element => {
      selectedOptions[element] = element;
    });
    this.setState({ selectedOptions });
  }

  render() {
    const { data } = this.props;
    const { selectedOptions } = this.state;

    return (
      <Fragment>
        <Checkbox.Group
          id="modal-check-group"
          options={this.checkOptions}
          defaultValue={[CLOSEMARKET_VARNAME]}
          onChange={this.onCheckGroupChange}
        />
        <div className="modal-chart-box">
          <ResponsiveContainer>
            <AreaChart data={data} margin={{ top: 30, right: 0, left: 0, bottom: 0 }}>
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="date" />
              <YAxis />
              <Tooltip />
              {selectedOptions[MARKETCAP_VARNAME] && (
                <Area
                  type="monotone"
                  dataKey={MARKETCAP_VARNAME}
                  stroke="#84D8FF"
                  fill="#84D8FF"
                />
              )}
              {selectedOptions[CLOSEMARKET_VARNAME] && (
                <Area
                  type="monotone"
                  dataKey={CLOSEMARKET_VARNAME}
                  stroke="#FFE05B"
                  fill="#FFE05B"
                />
              )}
              {selectedOptions[HIGHMARKET_VARNAME] && (
                <Area
                  type="monotone"
                  dataKey={HIGHMARKET_VARNAME}
                  stroke="#6BE8C5"
                  fill="#6BE8C5"
                />
              )}
              {selectedOptions[VOLUME_VARNAME] && (
                <Area
                  type="monotone"
                  dataKey={VOLUME_VARNAME}
                  stroke="#C755FF"
                  fill="#C755FF"
                />
              )}
            </AreaChart>
          </ResponsiveContainer>
        </div>
      </Fragment>
    );
  }
}

export default injectIntl(CryptoChart);

CryptoChart.propTypes = {
  intl: PropTypes.object.isRequired,
  data: PropTypes.array
};
