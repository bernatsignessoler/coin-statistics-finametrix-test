import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';

import translator from '../../../../core/utils/translationUtil';

import CryptoChart from './components/CryptoChart';

import { Modal, Alert, Select, Statistic } from 'antd';

import './styles.css';

import { TOP_TEN_CRYPTOCURRENCIES } from '../../../../core/constants/alphavantage';

const { Option } = Select;

class CryptoModal extends PureComponent {
  render() {
    const {
      intl,
      crypto,
      modalVisible,
      onCloseModal,
      onCryptoCurrencyChange
    } = this.props;
    const {
      currencyCode,
      Note,
      currencyName,
      marketName,
      lastCurrencyDate,
      dailyCurrency
    } = crypto;

    return (
      <Modal
        title={
          Note ? (
            currencyCode
          ) : (
            <Statistic
              className="modal-title-box"
              value={currencyName}
              suffix={`${translator.defaultTranslate(
                'lastUpdate',
                intl
              )} ${lastCurrencyDate}`}
            />
          )
        }
        width={'60%'}
        visible={modalVisible}
        destroyOnClose
        onCancel={onCloseModal}
        footer={null}
      >
        <div className="modal-body-wrapper">
          <Fragment>
            <div className="modal-filters-wraper">
              {!Note && (
                <Statistic
                  title={translator.defaultTranslate('ExchangeMarket', intl)}
                  value={marketName}
                />
              )}
              <Select
                className="modal-select-box"
                onChange={onCryptoCurrencyChange}
                placeholder={translator.defaultTranslate(
                  'selectCryptocurrencyPlaceholder',
                  intl
                )}
              >
                {TOP_TEN_CRYPTOCURRENCIES.map(code => (
                  <Option key={code} exvalue={code}>
                    {code}
                  </Option>
                ))}
              </Select>
            </div>
            {Note ? (
              <Alert description={Note} type="warning" showIcon />
            ) : (
              <CryptoChart data={dailyCurrency} />
            )}
          </Fragment>
        </div>
      </Modal>
    );
  }
}

export default injectIntl(CryptoModal);

CryptoModal.propTypes = {
  intl: PropTypes.object.isRequired,
  crypto: PropTypes.object.isRequired,
  modalVisible: PropTypes.bool.isRequired,
  onCloseModal: PropTypes.func.isRequired,
  onCryptoCurrencyChange: PropTypes.func.isRequired
};
