import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import { Link } from 'react-router-dom';

import translator from '../../core/utils/translationUtil';

import { applyLanguageContext } from '../../core/contexts/languageContext';

import { ESP, ENG } from '../../core/constants/languages';

import { Menu, Icon } from 'antd';

import './styles.css';

const Navbar = ({ id, className, intl, language, onSelectLanguage }) => (
  <Menu id={id} className={className} mode="horizontal" theme="dark" selectedKeys={[]}>
    <Menu.Item>
      <Link to="/">{translator.defaultTranslate('home', intl)}</Link>
    </Menu.Item>
    <Menu.SubMenu id="language-navbar-menu-item" title={language.toUpperCase()}>
      <Menu.Item onClick={() => onSelectLanguage(ENG)}>
        {ENG === language && <Icon type="check" />}
        {translator.defaultTranslate('english', intl)}
      </Menu.Item>
      <Menu.Item onClick={() => onSelectLanguage(ESP)}>
        {ESP === language && <Icon type="check" />}
        {translator.defaultTranslate('spanish', intl)}
      </Menu.Item>
    </Menu.SubMenu>
  </Menu>
);

export default applyLanguageContext(injectIntl(Navbar));

Navbar.propTypes = {
  id: PropTypes.string,
  className: PropTypes.string,
  language: PropTypes.string.isRequired,
  onSelectLanguage: PropTypes.func.isRequired,
  intl: PropTypes.object.isRequired
};
