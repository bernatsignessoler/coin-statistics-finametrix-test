import React from 'react';

import Navbar from './Navbar';
import Main from './Main';

import './styles.css';

const Home = () => (
  <div className="home-wrapper">
    <Navbar id="home-navbar" />
    <div className="home-content-wrapper">
      <Main />
    </div>
  </div>
);

export default Home;
