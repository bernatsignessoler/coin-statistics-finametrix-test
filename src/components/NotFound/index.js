import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import { Link } from 'react-router-dom';

import translator from '../../core/utils/translationUtil';

import CsTitle from '../../core/uikit/CsTitle';

import './styles.css';

const NotFound = ({ intl }) => (
  <div className="not-found-wrapper">
    <CsTitle size="ex-large-cs-title-size">404 NOT FOUND</CsTitle>
    <Link to="/">{translator.defaultTranslate('notFound', intl)}</Link>
  </div>
);

export default injectIntl(NotFound);

NotFound.propTypes = {
  className: PropTypes.string,
  intl: PropTypes.object.isRequired
};
