# Changelog

All notable changes to this project will be documented in this file.
Each version should:

- Write all dates in YYYY-MM-DD format. (Example: 2012-06-02 for June 2nd, 2012.)
- `Group` changes to describe their impact on the project, as follows:
- `Added` for new features.
- `Changed` for changes in existing functionality.
- `Deprecated` for once-stable features removed in upcoming releases.
- `Removed` for deprecated features removed in this release.
- `Fixed` for any bug fixes.
- `Security` to invite users to upgrade in case of vulnerabilities.

### Added

- Modal with cryptocurrency details and configurable chart.
- Cryptocurrency List, filters and countdown to reload blocked petitions
- Translations, utils, constants, and contexts
- Routing structure, navbar, notfound page, layout grid, core/uikit
- Eslint dependecies and rules, prettier rules, changelog file
- init react-app structure

### Changed

- Deleted route for details

### Fix
